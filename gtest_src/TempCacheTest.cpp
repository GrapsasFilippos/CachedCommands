/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 18/09/2018
 */
#include <gtest/gtest.h>

#include <iostream>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include "TempCache.hpp"


namespace {

class TempCacheTest : public ::testing::Test {
    protected:
        std::string command = "echo -n CachedCommand";
        std::string * cachedContent = nullptr;
        TempCache tempCache;

        void SetUp() override
        {
            boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::error);
        }
};


TEST_F(TempCacheTest, GetItemNullptr)
{
    try {
        cachedContent = tempCache.getItem(command);
    } catch (...) {
    }

    EXPECT_EQ(nullptr, cachedContent);
}

TEST_F(TempCacheTest, SetGetItem)
{
    try {
        tempCache.setItem(command, "CachedCommand", 10000);
        cachedContent = tempCache.getItem(command);
    } catch (...) {
    }

    EXPECT_STREQ("CachedCommand", cachedContent->c_str());
}

} // namespace
