/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 17/09/2018
 */
#include <gtest/gtest.h>

#include <iostream>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include "com/grapsas/crypto/CryptoPPHelper.hpp"


namespace {

TEST(CryptoPPHelperTest, sha1)
{
    boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::error);
    com::grapsas::crypto::CryptoPPHelper cryptoPPHelper;

    EXPECT_STRCASEEQ(cryptoPPHelper.hash("ls -lah").c_str(), "c60ff1827c8451241f51436cef6463ef3193b5a3");
}

} // namespace
