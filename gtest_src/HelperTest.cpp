/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 17/09/2018
 */
#include <gtest/gtest.h>

#include <iostream>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include "Helper.hpp"


namespace {

TEST(HelperTest, exec)
{
    boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::error);
    Helper helper;

    EXPECT_STREQ(helper.exec(std::string("echo -n CachedCommands").c_str()).c_str(), "CachedCommands");
}

} // namespace
