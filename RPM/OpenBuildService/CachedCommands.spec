Name:           CachedCommands
Version:        1.0.0
Release:        1
Summary:        The main purpose of this program is to "play" with C++ and help the immediate return of the cached response of the given slow response command.
License:        MIT License
Url:            https://gitlab.com/GrapsasFilippos/CachedCommands
Source:         CachedCommands-v1.0.0.tar.bz2
BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  libboost_headers1_68_0-devel
BuildRequires:  libboost_log1_68_0-devel
BuildRequires:  libboost_program_options1_68_0-devel
BuildRequires:  libboost_filesystem1_68_0-devel
BuildRequires:  libboost_system1_68_0-devel
BuildRequires:  libcryptopp-devel
Requires:       libcryptopp5_6_5
BuildRequires:  gtest
BuildRoot:      %{_tmppath}/%{name}-v%{version}-build


%description
The main purpose of this program is to "play" with C++ and help the immediate return of the cached response of the given slow response command.


%prep
%setup -q -n CachedCommands-v1.0.0

%build
cd build/
cmake ../
make %{?_smp_mflags}

%install
cd build/
%make_install

%files
%defattr(-,root,root)
%doc
%license LICENSE
/usr/local/bin/ccmd

%changelog
