/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 27/08/2018
 */
#include "TempCache.hpp"

#include <iostream>
#include <fstream>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

#include "com/grapsas/crypto/CryptoPPHelper.hpp"
#include "com/grapsas/exceptions/CacheNotExistsException.hpp"
#include "com/grapsas/exceptions/CacheExpiredException.hpp"
#include "com/grapsas/exceptions/IOException.hpp"


using namespace std;

using namespace boost::posix_time;

using namespace com::grapsas::crypto;
using namespace com::grapsas::exceptions;

void TempCache::setItem(std::string name, std::string value, unsigned long int expire)
{
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - "
        << name << ", " << value << ", " << expire;
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << STR_TIME_SIZE;

    ptime pTimeExpire = microsec_clock::universal_time() + milliseconds(expire);
    string st = to_iso_string(pTimeExpire);
    char at [STR_TIME_SIZE];
    strncpy(at, st.c_str(), STR_TIME_SIZE);
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << to_iso_string(pTimeExpire);
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << at;
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << TMP_DIR;

    dirGuard();
    CryptoPPHelper cryptoPPHelper;
    string fName = cryptoPPHelper.hash(name) + ".cache";
    string fPath = TMP_DIR + fName;
    fstream fStream(fPath, ios::out | ios::trunc);
    if (!fStream.is_open()) {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Unable to open file";
        throw IOException("Unable to open file");
    }
    fStream.write(at, STR_TIME_SIZE);
    fStream.write(value.c_str(), value.size());

    fStream.close();
}

std::string * TempCache::getItem(std::string name)
{
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << name;

    string * returnStr = nullptr;
    CryptoPPHelper cryptoPPHelper;
    string fName = cryptoPPHelper.hash(name) + ".cache";
    string fPath = TMP_DIR + fName;
    char at [STR_TIME_SIZE];
    string st;
    dirGuard();

    if (!boost::filesystem::exists(fPath)) {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Cache not exists";
        throw CacheNotExistsException();
    }

    fstream fStream(fPath, ios::in);
    if (!fStream.is_open()) {
        BOOST_LOG_TRIVIAL(error) << __PRETTY_FUNCTION__ << " - Unable to open file";
        throw IOException("Unable to open file");
    }
    stringstream stringStream;

    fStream.seekg(0, ios::end);
    unsigned int fileLength = fStream.tellg();
    fStream.seekg(0, ios::beg);
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - fileLength: " << fileLength;

    fStream.read(at, STR_TIME_SIZE);
    st = string(at, STR_TIME_SIZE);
    ptime cachePTime(from_iso_string(st));
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - cachePTime: " << to_iso_string(cachePTime);
    ptime currentPTime = microsec_clock::universal_time();
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - currentPTime: " << to_iso_string(currentPTime);
    time_duration pTimeDiff = (cachePTime - currentPTime);
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - pTimeDiff (milliseconds): " << pTimeDiff.total_milliseconds();
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - pTimeDiff (seconds): " << pTimeDiff.total_seconds();

    if (pTimeDiff.total_milliseconds() < 0) {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Expired cache";
        fStream.close();
        throw CacheExpiredException();
    }

    stringStream << fStream.rdbuf();
    returnStr = new string(stringStream.str());
    fStream.close();

    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - at: " << at;
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - st: " << st;
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - stringStream: " << * returnStr;

    return returnStr;
}

void TempCache::dirGuard()
{
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__;

    if (boost::filesystem::exists(TMP_DIR)){
        return;
    }

    if (!boost::filesystem::create_directory(TMP_DIR)) {
        throw IOException("Unable to create cache directory");
    }
}
