/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 27/08/2018
 */
#ifndef TEMPCACHE_HPP
#define TEMPCACHE_HPP

#include <iostream>
#include <unistd.h>

#include <boost/filesystem.hpp>

#include "Cache.hpp"


class TempCache : public Cache
{
    public:
        void setItem(std::string name, std::string value, unsigned long int expire) override;
        std::string * getItem(std::string name) override;
    private:
        const unsigned int STR_TIME_SIZE = 20;
        const std::string TMP_DIR = boost::filesystem::temp_directory_path().string() +
            "/CachedCommands-" + getlogin() + "/";
        void dirGuard();
};

#endif // TEMPCACHE_HPP
