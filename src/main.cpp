/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 22/08/2018
 */
#include <iostream>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include "com/grapsas/exceptions/CacheNotExistsException.hpp"
#include "com/grapsas/exceptions/CacheExpiredException.hpp"
#include "Helper.hpp"
#include "CmdOptions.hpp"
#include "TempCache.hpp"


using namespace std;

using namespace com::grapsas::exceptions;

int main(int argc, char* argv[])
{
    boost::log::core::get()
        ->set_filter(boost::log::trivial::severity >= boost::log::trivial::error)
    ;
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__;

    CmdOptions * cmdOptions = new CmdOptions(argc, argv);
    if (cmdOptions->breakRun()) {
        return 0;
    }

    Helper helper;
    boost::program_options::variables_map variablesMap = cmdOptions->getVm();
    string command = variablesMap["command"].as<string>();

    // CachedCommand
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - command: " << command;
    string * cachedContent = nullptr;
    TempCache tempCache;
    try {
        cachedContent = tempCache.getItem(command);
    } catch (CacheNotExistsException & e) {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Catch CacheNotExists";
    } catch (CacheExpiredException & e) {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Catch CacheExpired";
    }
    if (cachedContent == nullptr) {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Creating cachedContent";
        cachedContent = new string(helper.exec(command.c_str()));
        tempCache.setItem(command, * cachedContent, 10000);
    } else {
        BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - cachedContent loaded";
    }
    cout << * cachedContent;

    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Finishing";

    return 0;
}
