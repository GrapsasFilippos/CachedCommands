/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 06/09/2018
 */
#ifndef HELPER_HPP
#define HELPER_HPP


#include <iostream>

class Helper
{
    public:
        std::string exec(const char * cmd);
};

#endif
