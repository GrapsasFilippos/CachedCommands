/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 05/09/2018
 */
#ifndef SHA1HELPER_HPP
#define SHA1HELPER_HPP

#include <iostream>


namespace com { namespace grapsas { namespace crypto {

class SHA1Helper
{
    public:
        virtual std::string hash(std::string str) = 0;
};

}}}

#endif
