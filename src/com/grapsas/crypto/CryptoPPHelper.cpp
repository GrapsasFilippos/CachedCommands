/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 05/09/2018
 */
#include "CryptoPPHelper.hpp"

#include <iostream>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

#include <cryptopp/filters.h>
#include <cryptopp/sha.h>
#include <cryptopp/hex.h>


using namespace std;

namespace com { namespace grapsas { namespace crypto {

string CryptoPPHelper::hash(string str)
{
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << str;

    CryptoPP::SHA1 hash;
    string digest;

    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - Begin hashing...";
    CryptoPP::StringSource s(
        str, true,
        new CryptoPP::HashFilter(
            hash, new CryptoPP::HexEncoder(
                new CryptoPP::StringSink(
                    digest
                ) // StringSink
            ) // HexEncoder
        ) // HashFilter
    ); // StringSource
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - " << digest;

    return digest;
}

}}}
