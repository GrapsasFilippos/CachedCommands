/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 05/09/2018
 */
#ifndef CRYPTOPPHELPER_HPP
#define CRYPTOPPHELPER_HPP

#include <iostream>

#include "SHA1Helper.hpp"


namespace com { namespace grapsas { namespace crypto {

class CryptoPPHelper: public SHA1Helper
{
    public:
        std::string hash(std::string str);
};

}}}

#endif
