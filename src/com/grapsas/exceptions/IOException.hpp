/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 11/09/2018
 */
#ifndef COM_GRAPSAS_EXCEPTIONS_IOEXCEPTION_HPP
#define COM_GRAPSAS_EXCEPTIONS_IOEXCEPTION_HPP

#include <iostream>
#include <exception>


namespace com { namespace grapsas { namespace exceptions {

class IOException : public std::exception
{
    public:
        IOException();
        IOException(std::string errorMessage);
        virtual const char * what() const throw();
    private:
        std::string errorMessage;
};

}}}

#endif // COM_GRAPSAS_EXCEPTIONS_IOEXCEPTION_HPP
