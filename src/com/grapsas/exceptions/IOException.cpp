/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 11/09/2018
 */
#include "IOException.hpp"

#include <iostream>


namespace com { namespace grapsas { namespace exceptions {

using namespace std;

IOException::IOException() : std::exception()
{
    this->errorMessage = "IO Exception";
}

IOException::IOException(string errorMessage) : std::exception()
{
    this->errorMessage = errorMessage;
}

const char * IOException::what() const throw()
{
    return this->errorMessage.c_str();
}

}}}
