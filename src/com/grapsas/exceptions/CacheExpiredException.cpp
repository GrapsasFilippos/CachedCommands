/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 10/09/2018
 */
#include "CacheExpiredException.hpp"


namespace com { namespace grapsas { namespace exceptions {

const char * CacheExpiredException::what() const throw()
{
    return "Cache expired";
}

}}}
