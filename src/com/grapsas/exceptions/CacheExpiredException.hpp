/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 10/09/2018
 */
#ifndef COM_GRAPSAS_EXCEPTIONS_CACHEEXPIREDEXCEPTION_HPP
#define COM_GRAPSAS_EXCEPTIONS_CACHEEXPIREDEXCEPTION_HPP

#include <exception>


namespace com { namespace grapsas { namespace exceptions {

class CacheExpiredException : public std::exception
{
    public:
        virtual const char * what() const throw();
};

}}}

#endif // COM_GRAPSAS_EXCEPTIONS_CACHEEXPIREDEXCEPTION_HPP
