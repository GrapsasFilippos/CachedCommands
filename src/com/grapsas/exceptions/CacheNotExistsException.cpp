/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 10/09/2018
 */
#include "CacheNotExistsException.hpp"


namespace com { namespace grapsas { namespace exceptions {

const char* CacheNotExistsException::what() const throw()
{
    return "Cache not exists";
}

}}}
