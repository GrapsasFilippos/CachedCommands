/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 25/08/2018
 */
#include "CmdOptions.hpp"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

using namespace std;

CmdOptions::CmdOptions(int argc, char * argv[])
{
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__;

    breaked = false;
    cmdStr = "";

    try
    {
        boost::program_options::options_description options("Allowed options");
        options.add_options()
            ("help,h", "Produce help message")
            ("version,v", "Print version")
            ("command", boost::program_options::value<string>(&cmdStr), "Command to run and cache")
        ;

        boost::program_options::positional_options_description positional;
        positional.add("command", -1);

        boost::program_options::store(
            boost::program_options::command_line_parser(argc, argv)
                .options(options)
                .positional(positional)
                .run(),
            vm
        );
        boost::program_options::notify(vm);

        if (vm.count("help")) {
            cout << "Usage: ccmd [command]\n\n";
            cout << options << endl;
            breaked = true;
            return;
        }
        if (vm.count("version")) {
            cout << "CachedCommands, version 0.0.0" << endl;
            breaked = true;
            return;
        }
        if (cmdStr.compare("") == 0) {
            cout << "Please specify command.\nNothing to do. Exit\n";
            breaked = true;
            return;
        } else {
            BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - cmdStr: " << cmdStr;
            return;
        }
    }
    catch(exception& e) {
        breaked = true;
        cerr << e.what() << endl;
    }
}

bool CmdOptions::breakRun()
{
    return breaked;
}

boost::program_options::variables_map & CmdOptions::getVm()
{
    return vm;
}
