/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 27/08/2018
 */
#ifndef CACHE_HPP
#define CACHE_HPP

#include <iostream>


class Cache
{
    public:
        virtual void setItem(std::string name, std::string value, unsigned long int expire) = 0;
        virtual std::string * getItem(std::string name) = 0;
};
#endif // CACHE_HPP
