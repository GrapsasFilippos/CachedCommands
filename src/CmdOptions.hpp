/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 25/08/2018
 */
#ifndef CMDOPTIONS_HPP
#define CMDOPTIONS_HPP

#include <iostream>
#include <boost/program_options.hpp>


class CmdOptions
{
    public:
        CmdOptions(int argc, char * argv[]);
        bool breakRun();
        boost::program_options::variables_map & getVm();
    private:
        bool breaked;
        boost::program_options::variables_map vm;
        std::string cmdStr;
};
#endif // CMDOPTIONS_HPP
