/**
 * Copyright Filippos A. Grapsas <public001@grapsas.com>
 * Created at: 06/09/2018
 */
#include "Helper.hpp"

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>


using namespace std;

// https://stackoverflow.com/a/478960/2119070
string Helper::exec(const char * cmd)
{
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << " - cmd: " << cmd;

    std::array<char, 128> buffer;
    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        return ""; // TODO: Throw
    }

    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    BOOST_LOG_TRIVIAL(trace) << __PRETTY_FUNCTION__ << "\n" << result;

    return result;
}
