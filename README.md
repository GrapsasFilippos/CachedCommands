# CachedCommand

The main purpose of this program is to "play" with C++ and help the immediate return of the cached response of the given slow response command.

### Build

    cd CachedCommands/build
    cmake ../
    make

### Run

    cd CachedCommands/build
    ./ccmd --command="ls -lah"

### Google Test

    cd CachedCommands/build
    ./gtests

### Install
#### openSUSE Tumbleweed

    zypper addrepo https://download.opensuse.org/repositories/home:GrapsasFilippos/openSUSE_Tumbleweed/home:GrapsasFilippos.repo
    zypper refresh
    zypper install CachedCommands
